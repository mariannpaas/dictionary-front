
let words = [];
window.addEventListener('DOMContentLoaded', async () => {

    doLoadWords();
    words = await fetchWords();
});


async function doLoadWords() {
    words = await fetchWords();
    displayWords(words);
}

async function doDeleteWord(wordNr) {
    if (confirm('Kustuta sõna?')) {
        await removeWord(wordNr);
        await doLoadWords();
    }
}

function displayWords(words) {
    const mainElement = document.querySelector('main');
    let wordsHtml = '';

    for (const word of words) {
        if (word.inglise2 != null) {
            wordsHtml += /*html*/`
                <div>
                    <div class="word-item-property">
                     <div class="text-yellow">${word.eesti}</div>
                     <div>${word.inglise}</div>
                     
                     <div>${word.inglise2}</div> 
                    <div>
                    <button class="btn btn-secondary btn-sm" onclick="displayWordEditPopup(${word.nr})">Muuda</button>
                        <button class="btn btn-secondary btn-sm"class="button" onclick="doDeleteWord(${word.nr})">Kustuta</button>
                    </div><br>
                </div>
            `;
        } else {
            wordsHtml += /*html*/`
                <div>
                    <div class="word-item-property">
                     <div class="text-yellow">${word.eesti}</div>
                     <div>${word.inglise}</div>
                    <div>
                    <button class="btn btn-secondary btn-sm" onclick="displayWordEditPopup(${word.nr})">Muuda</button>
                        <button class="btn btn-secondary btn-sm"class="button" onclick="doDeleteWord(${word.nr})">Kustuta</button>
                    </div><br>
                </div>
            `;
        }
    }

    mainElement.innerHTML = wordsHtml;
}

async function displayWordEditPopup(wordsNr) {
    await openPopup(POPUP_CONF_500_500, 'wordsEditTemplate');

    if (wordsNr > 0) {
        const words = await fetchSingleWord(wordsNr);
        document.querySelector('#wordsNr').value = words.nr;
        document.querySelector('#wordsEesti').value = words.eesti;
        document.querySelector('#wordsInglise').value = words.inglise;
        document.querySelector('#wordsInglise2').value = words.inglise2;
    }
}

async function editWords() {
    const validationResult = validateWordsForm();

    if (validationResult.length == 0) {
        let word;
        if (document.querySelector('#wordsNr').value > 0) {
            word = {
                nr: document.querySelector('#wordsNr').value,
                eesti: document.querySelector('#wordsEesti').value,
                inglise: document.querySelector('#wordsInglise').value,
                inglise2: document.querySelector('#wordsInglise2').value,
            };
            await putWord(word);
        } else {

            word = {
                eesti: document.querySelector('#wordsEesti').value,
                inglise: document.querySelector('#wordsInglise').value,
                inglise2: document.querySelector('#wordsInglise2').value,
            };
            await postWord(word);
        }
        await doLoadWords();
        await closePopup();
    } else {
        displayWordsFormErrors(validationResult);
    }
}

function validateWordsForm() {
    let errors = [];

    let wordsEesti = document.querySelector('#wordsEesti').value;
    let wordsInglise = document.querySelector('#wordsInglise').value;
    let wordsInglise2 = document.querySelector('#wordsInglise2').value;

    if (wordsEesti.length < 2) {
        errors.push('Eestikeelse sõna kas määramata või liiga lühike (min 2 tähemärki)!');
    }
    if (wordsInglise.length < 2) {
        errors.push('Inglisekeelse sõna kas määramata või liiga lühike (min 2 tähemärki)!');
    }

    return errors;
}

function displayWordsFormErrors(errors) {
    const errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'block';

    let errorsHtml = '';
    for (let errorMessage of errors) {
        errorsHtml += /*html*/`<div>${errorMessage}</div>`;
    }

    errorBox.innerHTML = errorsHtml;
}

function filterWords() {
    const searchText = document.querySelector('#searchText');

    const filteredWords = words.filter(c => c.eesti.toLowerCase().includes(searchText.value.toLowerCase()));
    displayWords(filteredWords);
}