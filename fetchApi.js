async function fetchWords() {
    try {
        const response = await fetch(`${API_URL}/words/all`, {
            method: 'GET',
        });

        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function fetchSingleWord(wordsNr) {
    try {
        const response = await fetch(`${API_URL}/words/${wordsNr}`, {
            method: 'GET',
            headers: {
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function removeWord(WordNr) {
    try {
        const response = await fetch(`${API_URL}/words/${WordNr}`, {
            method: 'DELETE',
            headers: {
            }
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR OCCURRED', e);
    }
}

async function postWord(word) {
    try {
        const response = await fetch(`${API_URL}/words/add`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(word)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN ADDING WORD', e);
    }
}

async function putWord(word) {
    try {
        const response = await fetch(`${API_URL}/words/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(word)
        });
        return await processProtectedResponse(response);
    } catch (e) {
        console.log('ERROR IN MODIFYING WORD', e);
    }
}

async function processProtectedResponse(response) {
    if (response.status >= 400 && response.status <= 403) {
        throw new Error('Unauthorized');
    } else {
        return await response.json();
    }

}